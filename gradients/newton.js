 export default function () {
  return (problem, xs) => {
    let gradient = problem.gradient(xs);
    let hesian = problem.detHesian(xs);
    let ys = xs.map((x, i) => x + (-gradient[i]) / hesian[i]);
    let zs = problem.boundaries(ys);
    //console.log(gradient, hesian, xs, ys, zs)
    //console.log(problem.f(zs))
    return zs;
  }
}