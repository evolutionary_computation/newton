export default function(options){
  return (problem, xs) => {
    let gradient = problem.gradient(xs);

    xs = xs.map((x, i) => {
      options.delta[i] = options.miu * options.delta[i] - gradient[i] * options.alpha;
      return x + options.delta[i];
    })
    return problem.boundaries(xs);
  }
}