export default function(options){

  return (problem, xs) => {
    let gradient = problem.gradient(xs);
    xs = xs.map((x, i) => x + (-gradient[i]) * options.h);
    return problem.boundaries(xs);
  }
}