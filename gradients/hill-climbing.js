export default function(options){

  return (problem, xs) => {
    let ys = xs.map(x => x + options.sigma * options.rand());
    ys = problem.boundaries(ys);
    //console.log(problem.f(ys))
    return problem.f(xs) < problem.f(ys) ? xs : ys;
  }
}