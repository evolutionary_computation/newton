export newton from './newton'
export newtonDesc from './newton-desc'
export newtonMomemtum from './newton-momemtum'
export hillClimbing from './hill-climbing'