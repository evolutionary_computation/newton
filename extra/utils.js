export function zeros(n){
  return Array.apply(null, Array(n)).fill(0);
}

export function mean(elems){
  return elems.reduce((total, b) => total + b, 0) / elems.length;
}

export function worst(elems){
  return Math.max.apply(null, elems);
}

export function best(elems){
  return Math.min.apply(null,elems);
}

export function median(elems){
  elems = elems.sort();
  let half = parseInt(elems.length / 2);
  if(elems.length % 2 === 1){
    return elems[half]
  }
  else{
    return (elems[half] + elems[half-1]) / 2;
  }
}