// Standard Normal variate using Box-Muller transform.
export function gauss(){
  return () => {
    let u = 0, v = 0;
    while(u === 0) u = Math.random(); //Converting [0,1) to (0,1)
    while(v === 0) v = Math.random();
    return Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );
  }
}

export function powerLaw(options){
  let operator, x;
  return () => {
    operator = Math.random() < 0.5 ? -1 : 1;
    x = Math.random();
    return operator * Math.pow(1 - x, 1 / (1 - options.alpha));
  }
}