import { zeros } from '../extra/utils';

export default class Problem{

  constructor(options) {
    this.size = options.size;
    this.limit = options.limit;
    this.dimension = zeros(this.size);
    this.out = -options.size;
    this.data = [];
  }

  boundaries(xs){
    if(!this.min || !this.max) return xs;
    return xs.map(x => x < this.min ? this.getRandom() : x > this.max ? this.getRandom() : x);
  }

  getRandom(){
    let length = this.max - this.min;
    this.out++;
    return this.min + Math.random() * length;
  }

  getRandomXs(){
    return this.dimension.map(x => this.getRandom(x));
  }

  factible = xs => false
}