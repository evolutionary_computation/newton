import Problem from './problem'

export default class Schwefel extends Problem{
  min = -500;
  max = 500;

  f(xs){
    return 418.9829*this.size - xs.reduce((sum, x) => sum + x*Math.sin(Math.sqrt(Math.abs(x))), 0)
  }

  gradient(xs){
    return xs.map((x) => {
      let ax = Math.abs(x)
      let sx = Math.sqrt(ax)
      return -1 * (Math.sin(sx) + Math.cos(sx)*x*x / (2*Math.pow(ax, 3/2)))
    })
  }

  detHesian(xs){
    return xs.map((x) => {
      let ax = Math.abs(x)
      let sx = Math.sqrt(ax)
      return -1 * (3*sx*Math.cos(sx) - ax*Math.sin(sx)) / (4*x)
    })
  }
}