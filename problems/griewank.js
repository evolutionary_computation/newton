import Problem from './problem'

export default class Griewank extends Problem{
  min = -600;
  max = 600;

  f(xs) {
    let prod = xs.reduce((total, x, i) => total * Math.cos(x / Math.sqrt(i+1)), 1);
    let sum = xs.reduce((total, x) => total + x*x/4000, 0);
    return sum - prod + 1;
  }

  getConstantPart(xs, i){
    let prod = 1;
    for(let j=0; j<xs.length; j++){
      if(j !== i) prod *= Math.cos(xs[j] / Math.sqrt(j+1))
    }
    return prod
  }

  gradient(xs){
    return xs.map((x, i) => x/2000 + this.getConstantPart(xs,i)*Math.sin(x/Math.sqrt(i+1)) / Math.sqrt(i+1))
  }

  detHesian(xs){
    return xs.map((x, i) => 1/(2000) + this.getConstantPart(xs,i)*Math.cos(x/Math.sqrt(i+1)) / (i+1))
  }
}