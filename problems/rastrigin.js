import Problem from './problem'

export default class Rastrigin extends Problem{
  min = -5.12;
  max = 5.12;

  f(xs){
    return xs.reduce((sum, x) => sum + x*x - 10*Math.cos(2*Math.PI*x), 10*this.size)
  }

  gradient(xs){
    return xs.map(x => 2*x + 20*Math.PI*Math.sin(2*Math.PI*x))
  }

  detHesian(xs){
    return xs.map(x => 2 + 40*Math.PI*Math.PI*Math.cos(2*Math.PI*x))
  }
}