import * as Util from './extra/utils'
import newtonRaphson from './methods/newton-raphson'
import experiments from './experiments'
import fs from 'fs'

const N = 100

for(let experiment in experiments){
  let e = experiments[experiment];
  for(let gradient in e.gradients){
    let problems = []
    let limit = 0
    for(let i=0; i<N; i++){
      let problem = e.problem()
      problems.push(problem)
      newtonRaphson(problem, e.gradients[gradient])
      limit += problem.out
    }
    let csv = ['worst,best,mean,median,limit']
    let problem = problems[0];
    for(let j=0; j<problem.limit; j++){
      let values = [];
      for(let i=0; i<N; i++){
        values.push(problems[i].data[j])
      }
      csv.push([
        Util.worst(values),
        Util.best(values),
        Util.mean(values),
        Util.median(values),
        problem.out / N
      ].join(','))
    }
    fs.writeFileSync(`results/${experiment}_${gradient}.csv`, csv.join('\n'))
  }
}