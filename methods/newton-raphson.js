export default function(problem, gradient) {
  let count = 0;
  let xs = problem.getRandomXs();
  while (!problem.factible(xs) && count < problem.limit) {
    count++;
    xs = gradient(problem, xs);
    problem.data.push(problem.f(xs))
  }
  return xs;
}