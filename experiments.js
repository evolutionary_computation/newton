import * as Random from './extra/random'
import * as Util from './extra/utils'
import * as Problem from './problems'
import * as Gradient from './gradients'

export default {
  rastrigin2: {
    problem: () => new Problem.Rastrigin({size: 2, limit: 1000}),
    gradients: {
      newton          : Gradient.newton(),
      newtonDesc      : Gradient.newtonDesc({h: 0.67}),
      newtonMomemtum  : Gradient.newtonMomemtum({alpha: 0.1, miu: 0.68, delta: Util.zeros(2)}),
      hillGauss       : Gradient.hillClimbing({sigma: 2.5, rand: Random.gauss()}),
      hillPowerLaw    : Gradient.hillClimbing({sigma: 0.000000001, rand: Random.powerLaw({alpha: 1.1})})
    }
  },
  rastrigin10:{
    problem: () => new Problem.Rastrigin({size: 10, limit: 10000}),
    gradients: {
      newton          : Gradient.newton(),
      newtonDesc      : Gradient.newtonDesc({h: 0.64}),
      newtonMomemtum  : Gradient.newtonMomemtum({alpha: 0.1, miu: 0.71, delta: Util.zeros(10)}),
      hillGauss       : Gradient.hillClimbing({sigma: 1.5, rand: Random.gauss()}),
      hillPowerLaw    : Gradient.hillClimbing({sigma: 0.000000001, rand: Random.powerLaw({alpha: 1.2})})
    }
  },
  griewank2: {
    problem: () => new Problem.Griewank({size: 2, limit: 1000}),
    gradients: {
      newton          : Gradient.newton(),
      newtonDesc      : Gradient.newtonDesc({h: 0.64}),
      newtonMomemtum  : Gradient.newtonMomemtum({alpha: 0.3, miu: 0.71, delta: Util.zeros(2)}),
      hillGauss       : Gradient.hillClimbing({sigma: 1000, rand: Random.gauss()}),
      hillPowerLaw    : Gradient.hillClimbing({sigma: 0.000000001, rand: Random.powerLaw({alpha: 1.1})})
    }
  },
  griewank10: {
    problem: () => new Problem.Griewank({size: 10, limit: 10000}),
    gradients: {
      newton          : Gradient.newton(),
      newtonDesc      : Gradient.newtonDesc({h: 0.4}),
      newtonMomemtum  : Gradient.newtonMomemtum({alpha: 0.1, miu: 1, delta: Util.zeros(10)}),
      hillGauss       : Gradient.hillClimbing({sigma: 150, rand: Random.gauss()}),
      hillPowerLaw    : Gradient.hillClimbing({sigma: 0.000000001, rand: Random.powerLaw({alpha: 1.167})})
    }
  },
  schwefel2: {
    problem: () => new Problem.Schwefel({size: 2, limit: 1000}),
    gradients: {
      newton          : Gradient.newton(),
      newtonDesc      : Gradient.newtonDesc({h: 0.1}),
      newtonMomemtum  : Gradient.newtonMomemtum({alpha: 0.1, miu: 0.5, delta: Util.zeros(2)}),
      hillGauss       : Gradient.hillClimbing({sigma: 700, rand: Random.gauss()}),
      hillPowerLaw    : Gradient.hillClimbing({sigma: 0.000000001, rand: Random.powerLaw({alpha: 1.03})})
    }
  },
  schwefel10: {
    problem: () => new Problem.Schwefel({size: 10, limit: 10000}),
    gradients: {
      newton          : Gradient.newton(),
      newtonDesc      : Gradient.newtonDesc({h: 0.5}),
      newtonMomemtum  : Gradient.newtonMomemtum({alpha: 0.3, miu: 0.1, delta: Util.zeros(10)}),
      hillGauss       : Gradient.hillClimbing({sigma: 40, rand: Random.gauss()}),
      hillPowerLaw    : Gradient.hillClimbing({sigma: 0.000000001, rand: Random.powerLaw({alpha: 1.175})})
    }
  }
}



